Datacamp Scrapper
================

# Datacamp Scrapper

``` bash
ls -1 . | grep '.R$'
## get-slides.R
## get-videos.R
## list-courses.R
```

## List courses

``` bash
./list-courses.R --help
## Usage:
##   list-courses.R --help
##   list-courses.R
##   list-courses.R [options]
## 
## Options:
##   -h --help                   Show this message.
##   --technology <tech1,tech2>  Filter by technology.
##   --fields <field1,field2>    List of fields.
##   --filter <query>            Filter query.
##   --outfile <file>            Output file.
##   --format <format>           Output format [default: json].
```

Get R courses with
filter:

``` bash
./list-courses.R --technology r --format json --fields 'title,id' --filter 'Human Resources Analytics'
## [
##   {
##     "title": "Human Resources Analytics in R: Exploring Employee Data",
##     "id": 5977
##   },
##   {
##     "title": "Human Resources Analytics in R: Predicting Employee ...",
##     "id": 6325
##   }
## ]
```

## Fetch slides

``` bash
./get-slides.R --help
## Usage:
##   fetch-slides.R --help
##   fetch-slides.R [options]
## 
## Options:
##   -h --help                   Show this message.
##   --course-id <id>            Course ID.
##   --output-dir <dir>          Ouput directory.
```

Show links:

``` bash
./get-slides.R --course-id 5977
## Slides URLs:
##   - https://s3.amazonaws.com/assets.datacamp.com/production/course_5977/slides/chapter1.pdf
##   - https://s3.amazonaws.com/assets.datacamp.com/production/course_5977/slides/chapter2.pdf
##   - https://s3.amazonaws.com/assets.datacamp.com/production/course_5977/slides/chapter3.pdf
##   - https://s3.amazonaws.com/assets.datacamp.com/production/course_5977/slides/chapter4.pdf
##   - https://s3.amazonaws.com/assets.datacamp.com/production/course_5977/slides/chapter5.pdf
```

To download specify `output-dir` arg:

``` bash
./get-slides.R --course-id 5977 --output-dir <path-to-dest-dir>
```

## Fetch videos

``` bash
./get-videos.R --help
## Usage:
##   fetch-course.R --help
##   fetch-course.R [options]
## 
## Options:
##   -h --help                   Show this message.
##   --course-id <id>            Course ID.
##   --video-quality <level>     Video qulaity [default: auto].
##   --output-dir <dir>          Ouput directory.
```

Show links:

``` bash
./get-videos.R --course-id 5977
## Videos URLs:
## Chapter 1
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_1f1b78a54071ef29773cb5e0c0aa8a3c
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_a1c52bd79de1f53bb11b8a9b610ab9c5
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_60f9b307a3483cc6cd3d0498827cbcce
## Chapter 2
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_5c66d4dbbcbfb7d1bc64292a43121263
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_b8a296eb0f0c21c197d047f8e7535115
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_159bb0d2111163abb3df8dcfe0c7a4d3
## Chapter 3
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_f498bc0f7e685d11aff0f2b3a3d1857c
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_d6f1d75d499c3d1989ed3cae0dc6b437
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_79b327327397f1e40dff1d474cb77204
## Chapter 4
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_dfe4740416cdeb7142fa41853e8de55d
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_c64264e66a8ec56342c6e193fc96da81
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_e35910cb4be84caf9c6c5c8e3ac59707
## Chapter 5
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_973c4c7152c3fb502310338e3c5d733d
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_5422f75618bd493197cdb8fba73a127b
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_a77bd7f42e7c7cc8f7d2385a6dce0fec
##   - https://projector.datacamp.com/?quality=auto&video_hls=//videos.datacamp.com/transcoded/000_placeholders/v1/hls-temp.master.m3u8&projector_key=course_5977_53adede87a936416e06047975553199a
```

To download specify `output-dir` arg:

``` bash
./get-videos.R --course-id 5977 --output-dir <path-to-dest-dir>
```
